
// *****************  Subscribe Box  *************************

$(".subscribe-btn").click(function() {
  var sEmail = $('#txtEmail').val();
  if (validateEmail(sEmail)) {
    $(".popup > .popup-msg").html("CONGRATULATIONS!");
    $(".popup > .content").html("You have successfully Subscribed to our new Products");
    $(".close").attr('href' , '#latest-arrivals');
  }
  else {
    $(".popup > .popup-msg").html("WARNING!!!");
    $(".popup > .content").html("Invalid Input, Please fill out this Field");
    $(".close").attr('href' , '#subs');
  }
  
  });

  function validateEmail(sEmail) {
  var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
  if (filter.test(sEmail)) {
  return true;
  }
  else {
  return false;
  }
  };
   
 

// ***************** NAVIGATION BAR *************************



  $(window).scroll(function() {
    if ($(document).scrollTop() > 120) {
      $(".nav-items").addClass("nav-fixed");
    } else {
      $(".nav-items").removeClass("nav-fixed");
    }
  });


//*********** NAVIGATION MOBILE ICON ***************

  // function showIcon() {
  //   var x = document.getElementsByClassName("nav-list");
  //   if (x.style.display = "block") {
  //    x.style.display = "none";
  //   } else {
  //     x.style.display = "block";
  //   }
  // }

// *****************  BANNER SLIDER  *************************


$(document).ready(function(){
  $('.headers').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2500,
    fade: true,
    speed: 500,
    arrows:true,
    nextArrow: '<i class="fa fa-chevron-right slickarrowright"></i>',
    prevArrow: '<i class="fa fa-chevron-left slickarrowleft"></i>',
    dots: true,
    });
});

// *****************  LATEST ARRIVALS  *************************
$('.responsive').slick({
  dots: true,
  infinite: false,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 3,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
      // $(".slick-arrow").remove();
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

// *****************  MOST VIEWED  *************************

$(".most-viewed").slick({
  dots: true,
  infinite: true,
  slidesToShow:3,
  slidesToScroll: 3
});


// ***************** About-us  *************************

// $(".aboutme").slick({
//   dots: true,
//   infinite: true,
//   slidesToShow:1,
//   slidesToScroll: 1
// });


// *****************  CRAFTS  ***********************
        
$(document).ready(function(){
    $(".button").click(function(){
      var value = $(this).attr("data-filter");
      if(value=="all")
      {
        $(".filter").show(1000);						
      }else  
      {
        $(".filter").not(".all").hide(1000);
        $(".filter").filter("." + value).show(1000);
      };
      // active class
      $(".button").click(function()
      {
        $(this).addClass("act").siblings().removeClass("act");
      })										
    })		
  })


  // *********** Search Icon ****************


  function searchItem(){
    document.querySelector(".search-input").style.display = "inline-block";
    document.querySelector(".search").style.position = "absolute";
  }
  
// ***************** About-us Accordion *************************

  $(".accordion").on("click", ".accordion-header", function() {
    $(this).toggleClass("active").next().slideToggle();
    });


// ***************** Mobile-nav-icon *************************

function slide(){
  var x = document.getElementById('check-class');
  if( x.style.display === "none"){
    x.style.display = "block";
    
  } else {
    x.style.display = "none" ;
  }

}
  
// ************** IMAGE GALLERY FOR PRODUCT DETAIL ****************

var slideIndex = 1;
showSlides(slideIndex);

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
 for (i = 0; i < slides.length; i++) {
  slides[i].style.display = "none";
}

slides[slideIndex-1].style.display = "block";
 }
